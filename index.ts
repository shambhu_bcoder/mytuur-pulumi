import * as aws from '@pulumi/aws';


const keyPair = new aws.ec2.KeyPair("myKeyPair", {
      publicKey: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfD5w7eOylg4tjMH8kqqZ9zGi83UztADzXWvvFiy3raVTy5y82MJ2A9uqnaYYJmKFhkFp1Dcz6aopzhiyE4hVRpo0nQD8X8wqR3WZ+peENwO1BrbqhQRKL5xL5vgchqnwps6vdR4u4NxDHSYh0a2L8xRBmekbtqMbKMXkZanyHMMAkzmZOpjdpnZJvdOX+7mwaCsScbHdYzBfT4sFx3vjem8OHhcSxR6wBGr0Y1ZfGL+RTjg0jdc66Pj6TntHphV6E0HlJoKdeLcI12QrinQSs9T5REXdbS6XnQpHuuJ1gOWh14GrJmA3FzvNU/uqxszzgYO5lrKuNk5f8gtsaKj+J shambhunath@bcoder.in",
  });
// Create a new security group
const securityGroup = new aws.ec2.SecurityGroup('mytuur-security-group', {
      description: 'Allow incoming connections to the EC2 instance',
});

// Ingress rule for SSH
const sshIngress = new aws.ec2.SecurityGroupRule(
      'sshIngress',
      {
            type: 'ingress',
            fromPort: 22,
            toPort: 22,
            protocol: 'tcp',
            cidrBlocks: ['0.0.0.0/0'],
            securityGroupId: securityGroup.id,
      },
      { parent: securityGroup },
);

// Ingress rule for HTTP
const httpIngress = new aws.ec2.SecurityGroupRule(
      'httpIngress',
      {
            type: 'ingress',
            fromPort: 80,
            toPort: 80,
            protocol: 'tcp',
            cidrBlocks: ['0.0.0.0/0'],
            securityGroupId: securityGroup.id,
      },
      { parent: securityGroup },
);

// Ingress rule for HTTPS
const httpsIngress = new aws.ec2.SecurityGroupRule(
      'httpsIngress',
      {
            type: 'ingress',
            fromPort: 443,
            toPort: 443,
            protocol: 'tcp',
            cidrBlocks: ['0.0.0.0/0'],
            securityGroupId: securityGroup.id,
      },
      { parent: securityGroup },
);

// Ingress rule for your custom application
const appIngress = new aws.ec2.SecurityGroupRule(
      'appIngress',
      {
            type: 'ingress',
            fromPort: 3023,
            toPort: 3023,
            protocol: 'tcp',
            cidrBlocks: ['0.0.0.0/0'],
            securityGroupId: securityGroup.id,
      },
      { parent: securityGroup },
);

// Ingress rule for MongoDB
const mongoDbIngress = new aws.ec2.SecurityGroupRule(
      'mongoDbIngress',
      {
            type: 'ingress',
            fromPort: 27017,
            toPort: 27017,
            protocol: 'tcp',
            cidrBlocks: ['0.0.0.0/0'],
            securityGroupId: securityGroup.id,
      },
      { parent: securityGroup },
);


// Create an EC2 instance with Ubuntu 22.04
const instance = new aws.ec2.Instance('mytuur-instance', {
      instanceType: 't3.medium',
      ami: 'ami-0914547665e6a707c', // Ubuntu 22.04 LTS
      keyName: keyPair.keyName, // Associate the key pair
      vpcSecurityGroupIds: [securityGroup.id], // Associate the security group,
      monitoring: true, //enable monitoring enabled
});

// Create an Elastic IP and associate it with the instance
const eip = new aws.ec2.Eip('mytuur-eip', {
      instance: instance.id,
      vpc: true,
});

// Export the instance's public IP address and public DNS name
export const instancePublicIp = instance.publicIp;
export const instancePublicHostName = instance.publicDns;



/**
      userData,
 * 
 */


      // Create a new key pair

/** const sshKey = new tls.PrivateKey('myKeyPair', {
      algorithm: 'RSA',
      rsaBits: 4096,
});

const keyPair = new aws.ec2.KeyPair(
      'mytuur',
      {
            keyName: 'mytuur',
            publicKey: sshKey.publicKeyOpenssh,
      },
      { parent: sshKey },
); **/